import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) throws Exception{
        // 加载指定的模板文件, 指定一下模板所在文件
        Configuration con = new Configuration(Configuration.VERSION_2_3_0);
        con.setDirectoryForTemplateLoading(new File("templates"));
        con.setDefaultEncoding("utf-8");
        //提供数据
        Map root = new HashMap();
        root.put("name","xiaop");
        // 获取模板文件
        Template temp = con.getTemplate("test.ftl");
        Writer out = new OutputStreamWriter(new FileOutputStream("test.html"));
        temp.process(root,out);
        out.flush();
        out.close();


    }



}
