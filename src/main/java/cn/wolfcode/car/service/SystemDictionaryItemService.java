package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.query.SystemDictionaryItemQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface SystemDictionaryItemService {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionaryItem record);

    SystemDictionaryItem selectByPrimaryKey(Long id);

    List<SystemDictionaryItem> selectAll();

    int updateByPrimaryKey(SystemDictionaryItem record);

    PageInfo<SystemDictionaryItem> list(SystemDictionaryItemQueryObject qo);

    List<SystemDictionaryItem> selectParent();

    List<SystemDictionaryItem> selectByTypeId(int i);
}
