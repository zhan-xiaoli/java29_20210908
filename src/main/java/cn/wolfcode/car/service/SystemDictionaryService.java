package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionary;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface SystemDictionaryService {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionary record);

    SystemDictionary selectByPrimaryKey(Long id);

    List<SystemDictionary> selectAll();

    int updateByPrimaryKey(SystemDictionary record);

    PageInfo<SystemDictionary> list(QueryObject qo);


}
