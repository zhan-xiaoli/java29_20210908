package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IEmployeeService {
    int deleteByPrimaryKey(Long id);

    int insert(Employee record);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectAll();

    int updateByPrimaryKey(Employee record);

    PageInfo<Employee> list(EmployeeQueryObject qo);

    void batchDeleteByEmployee(Long[] ids);

    Employee selectByUsername(String username);
/* 调出的*/
    Workbook getWorkbook();


/*调入的模板下载*/
    Workbook getWorkbook_model();

    void impotXls(MultipartFile file) throws IOException;
}
