package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Appointment;
import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.query.AppointmentQueryObject;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface AppointmentService {
    int deleteByPrimaryKey(Long id);

    int insert(Appointment record);

    Appointment selectByPrimaryKey(Long id);

    List<Appointment> selectAll();

    int updateByPrimaryKey(Appointment record);
    PageInfo<Appointment> list(AppointmentQueryObject qo);
}
