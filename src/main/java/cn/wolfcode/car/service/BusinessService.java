package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.query.BusinessQueryObject;
import cn.wolfcode.car.query.SystemDictionaryItemQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface BusinessService {
    int deleteByPrimaryKey(Long id);

    int insert(Business record);

    Business selectByPrimaryKey(Long id);

    List<Business> selectAll();

    int updateByPrimaryKey(Business record);

    PageInfo<Business> list(BusinessQueryObject qo);
// 查询店面信息
    List<Business> selectMainStore(int i);
}
