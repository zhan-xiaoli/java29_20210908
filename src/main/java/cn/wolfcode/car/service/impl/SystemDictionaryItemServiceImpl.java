package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.mapper.SystemDictionaryItemMapper;
import cn.wolfcode.car.query.SystemDictionaryItemQueryObject;
import cn.wolfcode.car.service.SystemDictionaryItemService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictionaryItemServiceImpl implements SystemDictionaryItemService {
    @Autowired
    private SystemDictionaryItemMapper systemDictionaryItemMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return systemDictionaryItemMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SystemDictionaryItem record) {
        return systemDictionaryItemMapper.insert(record);
    }

    @Override
    public SystemDictionaryItem selectByPrimaryKey(Long id) {
        return systemDictionaryItemMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SystemDictionaryItem> selectAll() {
        return systemDictionaryItemMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(SystemDictionaryItem record) {
        return systemDictionaryItemMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<SystemDictionaryItem> list(SystemDictionaryItemQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<SystemDictionaryItem> list = systemDictionaryItemMapper.selectForList(qo);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @Override
    public List<SystemDictionaryItem> selectParent() {
        return systemDictionaryItemMapper.selectParent();
    }

    @Override
    public List<SystemDictionaryItem> selectByTypeId(int i) {
        return systemDictionaryItemMapper.selectByTypeId(i);
    }
}
