package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.mapper.EmployeeMapper;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.service.IEmployeeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return employeeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Employee record) {
//        进行md5的加密
//        干扰的方式就是salt
        if (record.getPassword() != null) {
            Md5Hash md5Hash = new Md5Hash(record.getPassword(),record.getName());
            record.setPassword(md5Hash.toString());
        }
        return employeeMapper.insert(record);
    }

    @Override
    public Employee selectByPrimaryKey(Long id) {
        return employeeMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Employee> selectAll() {
        return employeeMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(Employee record) {
        return employeeMapper.updateByPrimaryKey(record);
    }


    @Override
    public PageInfo<Employee> list(EmployeeQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), "id asc");
        List<Employee> employeePageInfo = employeeMapper.selectForList(qo);
        System.out.println(employeePageInfo);
        PageInfo pageInfo = new PageInfo(employeePageInfo);
        return pageInfo;
    }

    @Override
    public void batchDeleteByEmployee(Long[] ids) {
        employeeMapper.batcDeleByds(ids);
    }

    @Override
    public Employee selectByUsername(String username) {
        return employeeMapper.selectByUsername(username);
    }

    @Override
    public Workbook getWorkbook() {
        //创建一个XLS表格
        Workbook workbook = new HSSFWorkbook();
        //创建工作区
        Sheet sheet = workbook.createSheet("employee");
        Row headrow = sheet.createRow(0);
        headrow.createCell(0).setCellValue("用户名");
        headrow.createCell(1).setCellValue("姓名");
        headrow.createCell(2).setCellValue("年龄");
        //获取要导出的数据
        List<Employee> employees = employeeMapper.selectAll();
        //循环的写入表格中
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = employees.get(i);
            Row row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(employee.getName());
            row.createCell(1).setCellValue(employee.getUsername());
            row.createCell(2).setCellValue(employee.getAge());
        }
        return workbook;
    }

    // 调入工能模板下载
    @Override
    public Workbook getWorkbook_model() {
        //创建一个XLS表格
        Workbook workbook = new HSSFWorkbook();
        //创建工作区
        Sheet sheet = workbook.createSheet("employee");
        Row headrow = sheet.createRow(0);
        headrow.createCell(0).setCellValue("用户名");
        headrow.createCell(1).setCellValue("姓名");
        headrow.createCell(2).setCellValue("年龄");

        return workbook;
    }

    @Override
    public void impotXls(MultipartFile file) throws IOException {
        // 模板的解析
        //读取模板内容
        // 将数据保存在数据库
        Workbook workbook = new HSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        //getLastRowNum 获取到
        int maxline = sheet.getLastRowNum();
        for (int i = 1; i <= maxline; i++) {
            Employee employee = new Employee();
            Row row = sheet.getRow(i);


            if (row.getCell(0).getCellType() == CellType.NUMERIC) {
                double cell0 = row.getCell(0).getNumericCellValue();
                employee.setUsername("" + cell0);
            } else {
                //如果以后输入的不是数字
                String cell0 = row.getCell(0).getStringCellValue();
                employee.setUsername("" + cell0);
            }

            if (row.getCell(1).getCellType() == CellType.NUMERIC) {
                double cell1 = row.getCell(1).getNumericCellValue();
                employee.setName("" + cell1);
            } else {
                //如果以后输入的不是数字
                String cell1 = row.getCell(0).getStringCellValue();
                employee.setName("" + cell1);
            }
            double cell2 = row.getCell(2).getNumericCellValue();
            employee.setAge((int) cell2);
            insert(employee);

        }
    }
}
