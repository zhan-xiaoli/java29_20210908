package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.EmployeeRole;
import cn.wolfcode.car.mapper.EmployeeRoleMapper;
import cn.wolfcode.car.service.IEmployeeRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeeRoleServiceImpl implements IEmployeeRoleService {
    @Autowired
    private EmployeeRoleMapper employeeRoleMapper;
    @Override
    public int insert(EmployeeRole record) {
        return employeeRoleMapper.insert(record);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
  employeeRoleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<EmployeeRole> selectAll() {
        return employeeRoleMapper.selectAll();
    }

    @Override
    public void batchDeleteByEmployee(Long[] ids) {
        employeeRoleMapper.batcDeleByds(ids);
    }
}
