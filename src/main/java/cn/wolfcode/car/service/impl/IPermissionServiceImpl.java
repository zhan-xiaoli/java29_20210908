package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.domain.Permission;
import cn.wolfcode.car.mapper.PermissionMapper;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.service.IPermissionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class IPermissionServiceImpl implements IPermissionService {
    @Autowired
    private PermissionMapper permissionMapper;
    @Override
    public int deleteByPrimaryKey(Long id) {
        return permissionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Permission record) {
        return permissionMapper.insert(record);
    }

    @Override
    public Permission selectByPrimaryKey(Long id) {
        return permissionMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Permission> selectAll() {
        return permissionMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(Permission record) {
        return permissionMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<Permission> list(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(),"id asc");
        List<Permission> employeePageInfo = permissionMapper.selectForList(qo);
        System.out.println(11111);
        System.out.println(employeePageInfo);
        System.out.println(11111);
        PageInfo pageInfo = new PageInfo(employeePageInfo);
        return pageInfo;
    }

    @Override
    public List<Permission> selectByEmployee(Long id) {
        return permissionMapper.selectByEmployee(id);
    }
}
