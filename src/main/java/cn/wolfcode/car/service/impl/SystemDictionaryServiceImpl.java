package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.domain.SystemDictionary;
import cn.wolfcode.car.mapper.SystemDictionaryMapper;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.service.SystemDictionaryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictionaryServiceImpl implements SystemDictionaryService {
    @Autowired
    private SystemDictionaryMapper systemDictionaryMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return systemDictionaryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SystemDictionary record) {
        return systemDictionaryMapper.insert(record);
    }

    @Override
    public SystemDictionary selectByPrimaryKey(Long id) {
        return systemDictionaryMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SystemDictionary> selectAll() {
        return systemDictionaryMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(SystemDictionary record) {
        return systemDictionaryMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<SystemDictionary> list(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<SystemDictionary> list = systemDictionaryMapper.selectForList(qo);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }


}


