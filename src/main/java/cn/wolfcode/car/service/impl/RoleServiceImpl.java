package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.domain.Role;
import cn.wolfcode.car.mapper.RoleMapper;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.service.IRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public int deleteByPrimaryKey(Long id) {
        return roleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Role record) {
        return roleMapper.insert(record);
    }

    @Override
    public Role selectByPrimaryKey(Long id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Role> selectAll() {
        return roleMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(Role record) {
        return roleMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<Role> list(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(),"id asc");
        List<Role> employeePageInfo = roleMapper.selectForList(qo);
        PageInfo pageInfo = new PageInfo(employeePageInfo);
        return pageInfo;
    }

    @Override
    public List<Role> selectEmployeeid(Long id) {
        return roleMapper.selectEmployeeid(id);
    }

    @Override
    public List<String> getRoleByEmployeeId(Long id) {
        return roleMapper.getRoleByEmployeeId(id);
    }
}
