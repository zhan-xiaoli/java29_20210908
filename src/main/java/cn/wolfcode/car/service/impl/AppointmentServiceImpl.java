package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Appointment;
import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.mapper.AppointmentMapper;
import cn.wolfcode.car.query.AppointmentQueryObject;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.service.AppointmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppointmentServiceImpl implements AppointmentService {
    @Autowired
    private AppointmentMapper appointmentMapper;
    @Override
    public int deleteByPrimaryKey(Long id) {
        return appointmentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Appointment record) {
        return appointmentMapper.insert(record);
    }

    @Override
    public Appointment selectByPrimaryKey(Long id) {
        return appointmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Appointment> selectAll() {
        return appointmentMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(Appointment record) {
        return appointmentMapper.updateByPrimaryKey(record);
    }



    @Override
    public PageInfo<Appointment> list(AppointmentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Appointment> list = appointmentMapper.selectForList(qo);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }
}
