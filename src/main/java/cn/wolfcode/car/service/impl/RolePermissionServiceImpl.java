package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.RolePermission;
import cn.wolfcode.car.mapper.RolePermissionMapper;
import cn.wolfcode.car.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    private RolePermissionMapper rolePermissionMapper;
    @Override
    public int insert(RolePermission record) {
        return rolePermissionMapper.insert(record);
    }

    @Override
    public List<RolePermission> selectAll() {
        return rolePermissionMapper.selectAll();
    }

    @Override
    public void deleteByRole_id(Long id) {
        rolePermissionMapper.deleteByRole_id(id);
    }
}
