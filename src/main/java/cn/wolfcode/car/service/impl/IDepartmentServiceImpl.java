package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.mapper.DepartmentMapper;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.service.IDepartmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class IDepartmentServiceImpl implements IDepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;
    @Override
    public int deleteByPrimaryKey(Long id) {
        return departmentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Department record) {
        return departmentMapper.insert(record);
    }

    @Override
    public Department selectByPrimaryKey(Long id) {
        return departmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Department> selectAll() {
        return departmentMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(Department record) {
        return departmentMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<Department> list(QueryObject qo) {
        // 提供pageHelper
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Department> list = departmentMapper.selectForList(qo);
           PageInfo pageInfo = new PageInfo(list);
           return pageInfo;
    }


    @Override
    public Workbook getWorkbook() {
        //创建一个XLS表格
        Workbook workbook = new HSSFWorkbook();
        //创建工作区
        Sheet sheet = workbook.createSheet("employee");
        Row headrow = sheet.createRow(0);
        headrow.createCell(0).setCellValue("编号");
        headrow.createCell(1).setCellValue("姓名");
        headrow.createCell(2).setCellValue("号码");
        //获取要导出的数据
        List<Department> departments = departmentMapper.selectAll();
        //循环的写入表格中
        for (int i=0; i<departments.size(); i++){
            Department department = departments.get(i);
            Row row = sheet.createRow(i+1);
            row.createCell(0).setCellValue(department.getId());
            row.createCell(1).setCellValue(department.getName());
            row.createCell(2).setCellValue(department.getSn());

        }
        return workbook;
    }
}
