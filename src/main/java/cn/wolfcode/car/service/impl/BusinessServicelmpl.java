package cn.wolfcode.car.service.impl;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.mapper.BusinessMapper;
import cn.wolfcode.car.query.BusinessQueryObject;
import cn.wolfcode.car.service.BusinessService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BusinessServicelmpl implements BusinessService {
    @Autowired
    private BusinessMapper businessMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return businessMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Business record) {
        return businessMapper.insert(record);
    }

    @Override
    public Business selectByPrimaryKey(Long id) {
        return businessMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Business> selectAll() {
        return businessMapper.selectAll();
    }

    @Override
    public int updateByPrimaryKey(Business record) {
        return businessMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageInfo<Business> list(BusinessQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), "id asc");
        List<Business> list = businessMapper.selectForList(qo);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @Override
    public List<Business> selectMainStore(int i) {
        return businessMapper.selectMainStore(i);
    }


}