package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

public interface IDepartmentService {
    int deleteByPrimaryKey(Long id);

    int insert(Department record);

    Department selectByPrimaryKey(Long id);

    List<Department> selectAll();

    int updateByPrimaryKey(Department record);

    PageInfo<Department> list(QueryObject qo);

    Workbook getWorkbook();
}
