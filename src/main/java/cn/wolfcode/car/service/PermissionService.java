package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Permission;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface PermissionService {

    int deleteByPrimaryKey(Long id);

    int insert(Permission record);

    Permission selectByPrimaryKey(Long id);

    List<Permission> selectAll();

    int updateByPrimaryKey(Permission record);
    PageInfo<Permission> selectForList(QueryObject qo);


    List<Permission> selectByEmployee(Long id);


    List<Permission> selectByRoleId(Long id);

    List<String> getPermissionExpressionByEmployeeId(Long id);
}
