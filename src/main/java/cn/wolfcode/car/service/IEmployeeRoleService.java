package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.EmployeeRole;

import java.util.List;

public interface IEmployeeRoleService {
    int insert(EmployeeRole record);
    void deleteByPrimaryKey(Long id);
    List<EmployeeRole> selectAll();

    void batchDeleteByEmployee(Long[] ids);
}
