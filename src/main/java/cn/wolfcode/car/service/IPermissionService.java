package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Permission;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IPermissionService {
    int deleteByPrimaryKey(Long id);

    int insert(Permission record);

    Permission selectByPrimaryKey(Long id);

    List<Permission> selectAll();

    int updateByPrimaryKey(Permission record);

    PageInfo<Permission> list(QueryObject qo);

     List<Permission> selectByEmployee(Long id);


}
