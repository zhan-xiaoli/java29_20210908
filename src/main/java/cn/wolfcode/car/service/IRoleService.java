package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Role;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IRoleService {
    int deleteByPrimaryKey(Long id);

    int insert(Role record);

    Role selectByPrimaryKey(Long id);

    List<Role> selectAll();

    int updateByPrimaryKey(Role record);
    /* 分页查询*/
    PageInfo<Role> list(QueryObject qo);

    List<Role> selectEmployeeid(Long id);


    List<String> getRoleByEmployeeId(Long id);
}
