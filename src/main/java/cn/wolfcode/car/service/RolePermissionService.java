package cn.wolfcode.car.service;

import cn.wolfcode.car.domain.RolePermission;

import java.util.List;

public interface RolePermissionService {
    int insert(RolePermission record);

    List<RolePermission> selectAll();

    void deleteByRole_id(Long id);
}
