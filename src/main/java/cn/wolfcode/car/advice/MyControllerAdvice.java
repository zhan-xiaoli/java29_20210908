package cn.wolfcode.car.advice;


import cn.wolfcode.car.result.JsoResult;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class MyControllerAdvice {
    @ExceptionHandler(UnknownAccountException.class)
    public String UnknownAccountException(AuthorizationException e, HttpServletResponse response) throws IOException {

        e.printStackTrace();
        //把错误信息返回给浏览器
        response.setContentType("application/json;charset=utf-8");
        JsoResult jsonResult = JsoResult.error("用户名不存在");
        response.getWriter().write(JSON.toJSONString(jsonResult));
        return null;
    }

    @ExceptionHandler(IncorrectCredentialsException.class)
    public String IncorrectCredentialsException(AuthorizationException e, HttpServletResponse response) throws IOException {

        e.printStackTrace();
        //把错误信息返回给浏览器
        response.setContentType("application/json;charset=utf-8");
        JsoResult jsonResult = JsoResult.error("密码错误");
        response.getWriter().write(JSON.toJSONString(jsonResult));
        return null;
    }
}
