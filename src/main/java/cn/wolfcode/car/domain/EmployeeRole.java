package cn.wolfcode.car.domain;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmployeeRole {
    private Long employee_id;

    private Long role_id;

}