package cn.wolfcode.car.domain;

import lombok.*;
import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SystemDictionaryItem {
    private Long id;

    private String title;

    private Integer sequence;

    private Long type_id;

    private Long parent_id;


    private SystemDictionaryItem parent;

    public String getJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", this.id);
        map.put("title", this.title);
        map.put("sequence", this.sequence);
        map.put("type_id", this.type_id);
        map.put("parent", parent);
        return JSON.toJSONString(map);
    }
}