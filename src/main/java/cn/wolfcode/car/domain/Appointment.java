package cn.wolfcode.car.domain;

import cn.wolfcode.car.CharaUtil.JsonUtil;
import cn.wolfcode.car.staUsenum.AppointentStatusEnum;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Appointment {
    private Long id;

    private String ano;

    private Integer status;

    private SystemDictionaryItem category;

    private String info;

    private String contact_tel;

    private String contact_name;

    private Business business;

    private Date create_time;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date appointment_time;

    public  String getJson()throws Exception{
        return JsonUtil.convert(this,Appointment.class,"yyyy-MM-dd HH:mm");
    }

    // 枚举类型
public  String getStatusNmae(){
return AppointentStatusEnum.getName(this.status);
    }

}