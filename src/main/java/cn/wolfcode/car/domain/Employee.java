package cn.wolfcode.car.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Employee {

    private Long id;
    private String username;
    private String name;
    private String password;
    private String email;
    private Integer age;
    private boolean admin;
    private Long dept_id;

    private Department dept;
}