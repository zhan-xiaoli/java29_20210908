package cn.wolfcode.car.domain;

import com.alibaba.fastjson.JSON;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SystemDictionary {
    private Long id;

    private String sn;

    private String title;

    private String intro;
    public String getJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", this.id);
        map.put("sn", this.sn);
        map.put("title", this.title);
        map.put("intro", this.intro);
        return JSON.toJSONString(map);
    }


}