package cn.wolfcode.car.domain;

import com.alibaba.druid.support.json.JSONUtils;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Permission {
    private Long id;

    private String name;

    private String expression;
    public  String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",this.id);
        map.put("name",this.name);
        map.put("expression",this.expression);
        String json = JSONUtils.toJSONString(map);
        return  json;
    }

}