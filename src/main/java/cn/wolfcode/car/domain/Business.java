package cn.wolfcode.car.domain;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Business {
    private Long id;

    private String name;

    private String intro;

    private String scope;

    private String tel;

    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date open_date;

    private String license_img;

    private String license_number;

    private String legal_name;

    private String legal_tel;

    private String legal_idcard;

    private Boolean main_store;
}