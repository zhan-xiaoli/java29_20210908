package cn.wolfcode.car.controller;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/test")
public class TestController {
    @Autowired
    private DepartmentMapper departmentMapper;
    @RequestMapping("/test")
    public  String test(Model model){
        System.out.println("测试");
        List<Department> departments = departmentMapper.selectAll();
        model.addAttribute("departments",departments);
        return "t";
    }


    @RequestMapping("/tests")
    public  String tests(Model model){
        System.out.println("测试");
        List<Department> result = departmentMapper.selectAll();
        model.addAttribute("result ",result);
        return "department/list";
    }

}
