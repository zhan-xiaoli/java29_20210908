package cn.wolfcode.car.controller;


import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.IDepartmentService;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 孙涛
 */
@Controller
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    private IDepartmentService iDepartmentService;

    public void setiDepartmentSevice(IDepartmentService iDepartmentService) {
        this.iDepartmentService = iDepartmentService;
    }
// 部门查询
    @RequestMapping("/list")
    public  String list(@ModelAttribute("qo") QueryObject qo, Model model){
        System.out.println("po的"+qo);
        PageInfo<Department> pageInfo = iDepartmentService.list(qo);
        model.addAttribute("result",pageInfo);
        return "department/list";
    }
    // 部门新增/修改
    @RequestMapping("/saveOrUpdate")
    public  String add(Department department){
        //通过Id来判断
        if (department.getId() ==null){
            // 新增
            iDepartmentService.insert(department);
        }else {
            // 修改
            iDepartmentService.updateByPrimaryKey(department);
        }
      return "redirect:http://localhost:8080/department/list";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult delete(Long id){
        System.out.println("删除的id为"+id);
        iDepartmentService.deleteByPrimaryKey(id);
        return JsoResult.success(true);
    }


    // 調出
    @RequestMapping("/export")
    public void export(HttpServletResponse resp) throws IOException {
        //提交一个数据保存到xls的请求  JAVA 操作 XLS文件 需要设定响应头信息
        //在响应头信息中要设定保存的文件名，保存类型
        resp.setHeader("Content-Disposition","attachment;filename=xiaopu.xls");
        //需要操作XLS  WorkBook
        //获取数据
        Workbook workbook = iDepartmentService.getWorkbook();
        //保存，生成文件
        workbook.write(resp.getOutputStream());
    }


}
