package cn.wolfcode.car.controller;

import cn.wolfcode.car.CharaUtil.CharaUtil;
import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionary;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.query.BusinessQueryObject;
import cn.wolfcode.car.query.SystemDictionaryItemQueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.BusinessService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author 孙涛
 */
@Controller
@RequestMapping("/business")
public class BusinessConarytroller {
@Autowired
private BusinessService businessService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") BusinessQueryObject qo, Model model) {
        PageInfo<Business> list = businessService.list(qo);;
        model.addAttribute("result",list);
        return "/business/list";
    }
    @RequestMapping("/input")
    public String input(Long id, Model model) {
      if (id==null){

      }else {
          Business business = businessService.selectByPrimaryKey(id);
          model.addAttribute("currentBusiness",business);

      }
      return "business/input";
    }
    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult delete(Long id){
        System.out.println("删除的id为"+id);
        businessService.deleteByPrimaryKey(id);
        return JsoResult.success(true);
    }
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(MultipartFile file, Business business, HttpServletRequest request)throws Exception{
    if (business.getId() != null){
        // 处理老照片
        String oldimg = business.getLicense_img();
        String path = request.getServletContext().getRealPath("/");
        File file1 = new File(path+oldimg);
        file1.delete();
    }
    // 处理上传文件
        String filename = file.getOriginalFilename();
        String[] arr= filename.split("\\.");
        // 获取文件扩建名
        String ext = arr[arr.length-1];
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String sdate = sdf.format(date);
        int rand = (int) (Math.random()*889999+1000);
        StringBuffer sb = new StringBuffer();
        sb.append(sdate);
        sb.append(rand);
        sb.append(".");
        sb.append(ext);
        String newFilename = sb.toString();
        System.out.println("文件名"+newFilename);
        //图片上传位置：webapp中的img目录
        //找到img目录路径
        String realPath = request.getServletContext().getRealPath("/img/");
        File newFile = new File(realPath,newFilename);
        FileOutputStream outputStream = new FileOutputStream(newFile);
        InputStream inputStream = file.getInputStream();
        System.out.println("1");
        //读取数据的容器
        System.out.println("12");
        byte[] buf = new byte[100];
        //每次读取的长度
        int len = 0;
        while ((len = inputStream.read(buf)) != -1){
            outputStream.write(buf,0,len);
        }
        System.out.println("2");
        outputStream.close();
        //2.将图片地址添加到business对象中
        business.setLicense_img("/img/"+ newFilename);
        System.out.println("3");
        //3.处理文件中文乱码
        CharaUtil.convert(business, Business.class);
        System.out.println("121213");
        //4.持久化business对象
        if(business.getId() == null){
            System.out.println("4");
            businessService.insert(business);
        }else {
            System.out.println("到这里啦");
            businessService.updateByPrimaryKey(business);

        }
        return "redirect:/business/list";
    }




    }




