package cn.wolfcode.car.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.wolfcode.car.domain.Appointment;
import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.query.AppointmentQueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.AppointmentService;
import cn.wolfcode.car.service.BusinessService;
import cn.wolfcode.car.service.SystemDictionaryItemService;
import cn.wolfcode.car.staUsenum.AppointentStatusEnum;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentMapperService;
    @Autowired
    private SystemDictionaryItemService systemDictionaryItemSerives;
    @Autowired
    private BusinessService businessService;



    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") AppointmentQueryObject qo) {
        // 查询业务里面 为id1的
        List<SystemDictionaryItem> systemDictionaryItems = systemDictionaryItemSerives.selectByTypeId(1);
        AppointentStatusEnum[] values = AppointentStatusEnum.values();
        PageInfo<Appointment> list = appointmentMapperService.list(qo);
        List<Business> businesses = businessService.selectAll();

        model.addAttribute("businesses",businesses);
        model.addAttribute("category",systemDictionaryItems);
        model.addAttribute("statue",values);
        model.addAttribute("result",list);
        return "appointment/list";
    }
    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult saveOrUpdate(Long id) {
        System.out.println("id为"+id);
        appointmentMapperService.deleteByPrimaryKey(id);
        return JsoResult.success(null);
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Appointment appointment) {
        if (appointment.getId() == null) {
            appointment.setStatus(0);
            appointment.setCreate_time(new Date());
            String date = DateUtil.format(new Date(), "yyyyMMddHHmmss");
            String rand = RandomUtil.randomNumbers(5);
            appointment.setAno(date + rand);
            appointmentMapperService.insert(appointment);
        } else {
            appointmentMapperService.updateByPrimaryKey(appointment);
        }
        return "redirect:/appointment/list";
    }



    //修改订单状态
    @RequestMapping("/saveOrUpdate_async")
    @ResponseBody
    public JsoResult saveOrUpdate_async(Appointment appointment){
        if(appointment.getId() == null){
            appointment.setStatus(0);
            appointment.setCreate_time(new Date());
            String date = DateUtil.format(new Date(), "yyyyMMddHHmmss");
            String rand = RandomUtil.randomNumbers(5);
            appointment.setAno(date+rand);
            appointmentMapperService.insert(appointment);
        }else{
            appointmentMapperService.updateByPrimaryKey(appointment);
        }
        return JsoResult.success(null);
    }






    @RequestMapping("/cw")
    public  String cw(){
        System.out.println("到拦截了");
        return "/common/nopermission";
    }

}
