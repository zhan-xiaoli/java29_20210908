package cn.wolfcode.car.controller;

import cn.wolfcode.car.domain.*;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.IEmployeeRoleService;
import cn.wolfcode.car.service.IRoleService;
import cn.wolfcode.car.service.PermissionService;
import cn.wolfcode.car.service.RolePermissionService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IEmployeeRoleService employeeRoleService;
    @Autowired
    private RolePermissionService rolePermissionService;
    @Autowired
    private PermissionService permissionService;
    @RequestMapping("/list")
    public String list(@ModelAttribute("qo")QueryObject qo, Model model){
        PageInfo pageInfo = roleService.list(qo);
        model.addAttribute("result",pageInfo);
        List<Permission> permlist = permissionService.selectAll();
        System.out.println(pageInfo.getList().get(0));
        model.addAttribute("permList", permlist);
        return "/role/list";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult delete(Long id){
        roleService.deleteByPrimaryKey(id);
        rolePermissionService.deleteByRole_id(id);
        return JsoResult.success(null);
    }
    //保存或编辑
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Role role, Long[] ids) {
        System.out.println("角色ID:"+role.getId());
        System.out.println("ids:"+ids);
        if (role.getId() == null) {
            //插入员工信息
            System.out.println(role.getId());
            roleService.insert(role);
            //同时要在角色表中查询下一个该员工的角色身份信息
            Long roleId = role.getId();

            if (ids != null) {
                for (Long id : ids) {
                    rolePermissionService.insert(new RolePermission(roleId,id));
                }
            }
        } else {
            roleService.updateByPrimaryKey(role);
            //删除原有的角色
            rolePermissionService.deleteByRole_id(role.getId());
            //插入新角色
            if (ids != null) {
                for (Long id : ids) {
                    rolePermissionService.insert(new RolePermission(role.getId(),id));
                }
            }
        }
        return "redirect:/role/list";
    }

    @RequestMapping("/input")
    public String input(Long id, Model model) {
        List<Permission> permissions = permissionService.selectAll();
        model.addAttribute("permList", permissions);

        List<Role> roles = roleService.selectAll();
        model.addAttribute("roleList", roles);

        if (id == null) {

        } else {
            Role role = roleService.selectByPrimaryKey(id);
            model.addAttribute("role", role);
            List<Permission> list = permissionService.selectByRoleId(id);
            model.addAttribute("list", list);
        }
        return "role/input";
    }

}
