package cn.wolfcode.car.controller;

import cn.wolfcode.car.annotation.RequiredPermission;
import cn.wolfcode.car.domain.Permission;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.IPermissionService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/permission")
public class permissionController {
    @Autowired
    private IPermissionService permissionMapper;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private IPermissionService permissionService;
    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") EmployeeQueryObject qo, Model model) {
        PageInfo<Permission> pageInfo =permissionMapper.list(qo);
        System.out.println("pageInfo为"+pageInfo);
        model.addAttribute("result", pageInfo);
        return "permission/list";
    }
    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult delete(Long id){
        System.out.println("删除的id为"+id);
        permissionMapper.deleteByPrimaryKey(id);
        return JsoResult.success(true);
    }

    //权限的重新加载
    @RequestMapping("/reload")
    @ResponseBody
    public JsoResult reload() {
        //获取数据库中所有的权限(name,expression)
        List<Permission> permissions = permissionMapper.selectAll();
        Map<String, String> permap = new HashMap<>();
        //遍历数据库中所有的权限（permissions）
        for (Permission permission : permissions) {
            //把name,expression放到permap集合中
            permap.put(permission.getName(), permission.getExpression());
        }
        //只对Controller类操作。获取到Controller的Class，放到map中
        Map<String, Object> map = applicationContext.getBeansWithAnnotation(Controller.class);
        Collection<Object> controllers = map.values();
        System.out.println(" map.values()是：" + controllers);
        for (Object controller : controllers) {
            Class<?> clzz = controller.getClass();
            Method[] methods = clzz.getDeclaredMethods();
            for (Method method : methods) {
                RequiredPermission annotation = method.getAnnotation(RequiredPermission.class);
                if (annotation != null) {
                    String name = annotation.name();
                    String expression = annotation.expression();
                    if (permap.get(name) == null) {
                        permissionService.insert(new Permission(null, name, expression));
                    }
                }
            }
        }
        //刷新当前登录用户的用户信息，将权限重新加载

        //如果数据库中没有指定的额权限，直接返回空

        return JsoResult.success(null);
    }
    // 部门新增/修改
    @RequestMapping("/saveOrUpdate")
    public  String add(Permission department){
        System.out.println("获取的id为"+department.getId());
        //通过Id来判断
        if (department.getId() ==null){
            // 新增
            permissionMapper.insert(department);
        }else {
            // 修改
            permissionMapper.updateByPrimaryKey(department);
        }


        return "redirect:http://localhost:8080/permission/list";
    }


}
