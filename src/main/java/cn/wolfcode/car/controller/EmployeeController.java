package cn.wolfcode.car.controller;


import cn.wolfcode.car.annotation.RequiredPermission;
import cn.wolfcode.car.domain.*;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.*;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IEmployeeRoleService employeeRoleService;
    @Autowired
    private IPermissionService permissionService;


    @RequestMapping("/list")
/*   @RequiredPermission(name = "员工页面", expression = "employee:list")*/
    public String list(@ModelAttribute("qo") EmployeeQueryObject qo, Model model) {
        PageInfo<Employee> pageInfo =employeeService.list(qo);
        System.out.println("查询员工为"+pageInfo);

        model.addAttribute("result", pageInfo);
        //展示部门
        List<Department> deptlist = departmentService.selectAll();
        System.out.println("查询部门为"+deptlist);
        model.addAttribute("deptlist", deptlist);
        return "employee/list";
    }
 /*   @RequiredPermission(name = "员工删除", expression = "employee:delete")*/
    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult delete(Long id) {
        employeeService.deleteByPrimaryKey(id);
        return JsoResult.success(true);
    }

    @RequestMapping("/input")
   /* @RequiredPermission(name = "员工新增修改", expression = "employee:saveOrUpdate")*/
/*    @RequiredPermission(name = "员工新增修改",experssion = "employee:saveOrUpdate")*/
    public String input(Long id, Model model) {
        //得到部门的数据
        List<Department> departments = departmentService.selectAll();
        model.addAttribute("deptList", departments);
        //角色
        List<Role> roles = roleService.selectAll();
        System.out.println("roles的角色"+roles.toString());
        model.addAttribute("roleList", roles);
        if (id == null) {
            //新增
            // employeeService;
        } else {
            //编辑
            Employee employee = employeeService.selectByPrimaryKey(id);
            model.addAttribute("curremp", employee);
            List<Role> roleList = roleService.selectEmployeeid(id);
            model.addAttribute("selfRoles", roleList);
        }

        return "employee/input";
    }

    //保存，编辑
    //角色可以有多个，所以用数组接收id
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Employee employee, Long[] ids) {
        if (employee.getId() == null) {
            //插入员工信息
            employeeService.insert(employee);
            //同时要在角色表Role中插入一个该员工的角色身份信息
            Long employeeid = employee.getId();
            if (ids != null) {
                //如果id不为空，遍历他的角色id
                for (Long id : ids) {
                    employeeRoleService.insert(new EmployeeRole(employeeid, id));
                }
            }
        } else {
            employeeService.updateByPrimaryKey(employee);
            //把原有的删除，在加新的
            employeeRoleService.deleteByPrimaryKey(employee.getId());
            if (ids != null) {
                //如果id不为空，遍历他的角色id
                for (Long id : ids) {
                    employeeRoleService.insert(new EmployeeRole(employee.getId(), id));
                }
            }

        }
        return "redirect:http://localhost:8080/employee/list";
    }


    @RequestMapping("/batchdelete")
    @ResponseBody
    public JsoResult delete(Long[] ids) {
        employeeService.batchDeleteByEmployee(ids);
        employeeRoleService.batchDeleteByEmployee(ids);
        return JsoResult.success(null);
    }


    @RequestMapping("/login")
    @ResponseBody
    public JsoResult login(String username, String password, HttpSession session, String verCode, HttpServletRequest request){
        System.out.println("username的值为"+username);
        System.out.println("password的值为"+password);
        try{
            session.setAttribute("username",username);
            String sessionCode = (String) request.getSession().getAttribute("captcha");
            System.out.println("sessionCode的值为"+sessionCode);
            // 判断验证码
            if (verCode==null || !sessionCode.equals(verCode.trim().toLowerCase())) {
                return JsoResult.error("验证码不正确");
            }
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username,password);
            System.out.println("usernamePasswordToken的值为"+usernamePasswordToken.toString());
            SecurityUtils.getSubject().login(usernamePasswordToken);

            System.out.println("usernamePasswordToken的值为"+usernamePasswordToken);
            return JsoResult.success(null);

        }catch (UnknownAccountException e){
            e.printStackTrace();
            return JsoResult.error("用户名不存在");
        }catch (IncorrectCredentialsException e){
            e.printStackTrace();
            return JsoResult.error("密码错误");
        }
       /*UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username,password);
       SecurityUtils.getSubject().login(usernamePasswordToken);
       return JsonResult.success(null);*/

    }
    @RequestMapping("/logout")
    //@ResponseBody
    public String logout(HttpSession session){
        session.removeAttribute("currentuser");
        session.removeAttribute("userpermission");
        return "redirect:http://localhost:8080/login.html";
    }


// 調出
@RequestMapping("/export")
public void export(HttpServletResponse resp) throws IOException {
    //提交一个数据保存到xls的请求  JAVA 操作 XLS文件 需要设定响应头信息
    //在响应头信息中要设定保存的文件名，保存类型
    resp.setHeader("Content-Disposition","attachment;filename=xiaopu.xls");
    //需要操作XLS  WorkBook
    //获取数据
    Workbook workbook = employeeService.getWorkbook();
    //保存，生成文件
    workbook.write(resp.getOutputStream());
}

    @RequestMapping("/checkName")
    @ResponseBody
    public Map checkname(String username) throws IOException {
        // 返回为 true时 通过验证
        // 返回为 false是  不通过
        Employee employee = employeeService.selectByUsername(username);
        Map<String,Boolean>map = new HashMap<>();
        if (employee == null){
            map.put("valid",true);
        }else {
            map.put("valid",false);
        }
        return  map;

    }
  /*  export_model
    调入功能的模板下载*/
  @RequestMapping("/export_model")
  public void  export_model(HttpServletResponse resp) throws IOException {
      resp.setHeader("Content-Disposition","attachment;filename=xiaopu.xls");
      Workbook workbook = employeeService.getWorkbook_model();
      workbook.write(resp.getOutputStream());
  }
  /*  importXls
    调入模板下载*/
  @RequestMapping("/importXls")
  public String  importXls(MultipartFile file) throws IOException {
      System.out.println("上传开始");
      employeeService.impotXls(file);
      return "redirect:http://localhost:8080/employee/list";
  }



}