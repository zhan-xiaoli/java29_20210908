package cn.wolfcode.car.controller;

import cn.wolfcode.car.domain.SystemDictionary;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.query.SystemDictionaryItemQueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.SystemDictionaryItemService;
import cn.wolfcode.car.service.SystemDictionaryService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/systemDictionaryItem")
public class SystemDictionayLtemConarytroller {
    @Autowired
    private SystemDictionaryItemService service;
    @Autowired
    private SystemDictionaryService systemDictionaryService;

    @RequestMapping("/list")
    public String input(@ModelAttribute("qo") SystemDictionaryItemQueryObject qo, Model model) {
        List<SystemDictionary> systemDictionaries = systemDictionaryService.selectAll();
        model.addAttribute("systemDictionaryList",systemDictionaries);

        List<SystemDictionaryItem> systemDictionaryItems = service.selectParent();
        model.addAttribute("parentitems",systemDictionaryItems);

        PageInfo<SystemDictionaryItem> list = service.list(qo);
        model.addAttribute("result",list);
        return "systemDictionaryItem/list";
    }



    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(SystemDictionaryItem systemDictionaryItem) {
        if (systemDictionaryItem.getId() == null){
            service.insert(systemDictionaryItem);
        }else {
            service.updateByPrimaryKey(systemDictionaryItem);
        }
        return "systemDictionaryItem/list";

    }



    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult saveOrUpdate(Long id) {
        System.out.println("开始删除的id为"+id);
        service.deleteByPrimaryKey(id);
        return JsoResult.success(null);

    }







}
