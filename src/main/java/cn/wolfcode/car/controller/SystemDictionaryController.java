package cn.wolfcode.car.controller;


import cn.wolfcode.car.domain.SystemDictionary;
import cn.wolfcode.car.query.QueryObject;
import cn.wolfcode.car.result.JsoResult;
import cn.wolfcode.car.service.SystemDictionaryService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/systemDictionary")
public class SystemDictionaryController {
    @Autowired
    private SystemDictionaryService systemDictionaryService;

    @RequestMapping("/list")
    public String input(@ModelAttribute("qo") QueryObject qo, Model model) {
        //通过权限 来限定。有权限的用户才能访问
        Subject subject = SecurityUtils.getSubject();
        if (subject.isPermitted("systemDictionary:list")) {
            PageInfo<SystemDictionary> pageInfo = systemDictionaryService.list(qo);
            model.addAttribute("result", pageInfo);
            return "systemDictionary/list";
        } else {
            return "common/nopermission";
        }
    }


    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(SystemDictionary systemDictionary) {
        if (systemDictionary.getId() == null){
            systemDictionaryService.insert(systemDictionary);
        }else {
            systemDictionaryService.updateByPrimaryKey(systemDictionary);
        }
        return "common/nopermission";

    }
    @RequestMapping("/delete")
    @ResponseBody
    public JsoResult saveOrUpdate(Long id) {
        System.out.println("id为"+id);
        systemDictionaryService.deleteByPrimaryKey(id);
        return JsoResult.success(null);

    }



}
