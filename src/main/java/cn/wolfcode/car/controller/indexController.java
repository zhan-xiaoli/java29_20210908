package cn.wolfcode.car.controller;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.service.BusinessService;
import cn.wolfcode.car.service.SystemDictionaryItemService;
import cn.wolfcode.car.service.SystemDictionaryService;
import cn.wolfcode.car.service.impl.SystemDictionaryItemServiceImpl;
import cn.wolfcode.car.service.impl.SystemDictionaryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class indexController {
    @Autowired
    private BusinessService businessService;
    @Autowired
    private SystemDictionaryItemService service;
    @RequestMapping("/index.html")
    public  String show(Model model){
        //店面信息
        List<Business> business    = businessService.selectMainStore(1);
        model.addAttribute("mainstore",business.get(0));
// 业务分类
        List<SystemDictionaryItem> bus = service.selectByTypeId(1);
        model.addAttribute("systemDictionaryItems",bus);

        // 门店详情
        //门店详情
        List<Business> businessList = businessService.selectAll();
        model.addAttribute("businessList",businessList);

        return "index";
    }


}
