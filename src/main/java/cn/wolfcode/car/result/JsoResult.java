package cn.wolfcode.car.result;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JsoResult <T>{
    private  int code;
    public  boolean success;
    private  String msg;
    private  T data;
    public  static  <T> JsoResult success( T data){
        JsoResult jsoResult = new JsoResult<>();
        jsoResult.setSuccess(true);
        jsoResult.setData(data);
        return jsoResult;
    }
    public  static  <T> JsoResult error( String msg){
        JsoResult jsoResult = new JsoResult<>();
        jsoResult.setSuccess(false);
        jsoResult.setMsg(msg);
        return jsoResult;
    }


}
