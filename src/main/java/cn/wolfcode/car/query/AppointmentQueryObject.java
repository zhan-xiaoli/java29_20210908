package cn.wolfcode.car.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class AppointmentQueryObject extends  QueryObject {
    private String ano;
    private Long category_id;
    private Integer status;
    private Long business_id;
    private String contact_tel;
    private String contact_name;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endDate;

}
