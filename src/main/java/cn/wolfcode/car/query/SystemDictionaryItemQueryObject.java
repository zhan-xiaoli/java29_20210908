package cn.wolfcode.car.query;
import lombok.*;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SystemDictionaryItemQueryObject extends  QueryObject {
    private  Long type_id;
}
