package cn.wolfcode.car.query;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QueryObject {
    private  int currentPage =1;
    private  int pageSize = 5;
}
