package cn.wolfcode.car.CharaUtil;

import java.lang.reflect.Field;

public class CharaUtil {
    // 参数1 : 对那个类进行
    public static  void  convert(Object obj,Class clzz)throws  Exception{
        Field[] fields = clzz.getDeclaredFields();
        for (Field field:fields){
            if (field.getType()== String.class){
                field.setAccessible(true);
                String str = (String) field.get(obj);
                byte[] bytes = str.getBytes("ISO-8859-1");
                field.set(obj,new String(bytes,"utf-8"));
            }

        }
    }
}
