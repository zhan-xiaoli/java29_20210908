package cn.wolfcode.car.CharaUtil;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;


import javax.xml.crypto.Data;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 孙涛
 */ /*
获取实体类对象 封装到 里面信息
 */
public class JsonUtil {
    // 参数1 : 对那个类进行
    public static  String  convert(Object obj,Class clzz,String format)throws  Exception{
        Map<String,Object> map = new HashMap<>();
        Field[] fields = clzz.getDeclaredFields();
        for (Field field:fields){
            field.setAccessible(true);
            if (field.getType().equals(Data.class)){
                String sdate = DateUtil.format((Date) field.get(obj),format);
                map.put(field.getName(),sdate);
            }else {
                map.put(field.getName(),field.get(obj));
            }

        }
        return JSON.toJSONString(map);
    }
}
