package cn.wolfcode.car.realm;

import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.service.IEmployeeService;
import cn.wolfcode.car.service.IRoleService;
import cn.wolfcode.car.service.PermissionService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component //将MyRealm交给spring管理
public class MyRealm extends AuthorizingRealm {
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private PermissionService permissionService;
    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前的主体对象
        Employee employee = (Employee) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        if(employee.isAdmin()){
            info.addRole("admin");
            info.addStringPermission("*:*");
            System.out.println("info:"+info);
            return info;
        }
        //获取当前的用户角色
        List<String> rolesnlist = roleService.getRoleByEmployeeId(employee.getId());
        System.out.println("rolesnlist:"+rolesnlist);
        //获取当前用户的权限
        List<String> permissionList = permissionService.getPermissionExpressionByEmployeeId(employee.getId());
        System.out.println("permissionList:"+permissionList);
        //给当前用户添加角色和权限(授权)
        info.addRoles(rolesnlist);
        info.addStringPermissions(permissionList);
        return info;
    }
    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        Employee employee = employeeService.selectByUsername(usernamePasswordToken.getUsername());
        if(employee == null){
            return null;
        }else {
            //认证成功后得到employee对象（第一个参数）
            AuthenticationInfo info = new SimpleAuthenticationInfo(employee,
                    employee.getPassword(),
                    ByteSource.Util.bytes(employee.getName()),
                    this.getName());
            return info;
        }



    }
    //获取用户的密钥，盐 创建含有密钥的对象  进行登录验证
/*    @Override*/
/*    @Autowired*/
    @Autowired
    public void  setCredentialsMatcher(CredentialsMatcher credentialsMatcher){
        super.setCredentialsMatcher(credentialsMatcher);
    }

}
