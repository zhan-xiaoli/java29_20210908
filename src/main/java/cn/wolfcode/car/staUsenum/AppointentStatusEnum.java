package cn.wolfcode.car.staUsenum;

import lombok.Getter;
@Getter
public enum AppointentStatusEnum {
    PEND(0, "待确认"),
    PERFORM(1, "履行中"),
    CONSUME(2, "消费中"),
    FINISH(3, "归档"),
    FAILURE(4, "废弃");

    private Integer value;
    private String name;

    AppointentStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getName(Integer value) {
        AppointentStatusEnum[] values = values();
        for (AppointentStatusEnum appointmentStatusEnum : values) {
            if (appointmentStatusEnum.value.equals(value)) {
                return appointmentStatusEnum.name;
            }
        }
        return null;
    }


}
