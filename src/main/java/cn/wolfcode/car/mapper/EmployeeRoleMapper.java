package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.EmployeeRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeRoleMapper {
    int insert(EmployeeRole record);
    void deleteByPrimaryKey(Long id);
    List<EmployeeRole> selectAll();

    void batcDeleByds(@Param("ids") Long[] ids);
}