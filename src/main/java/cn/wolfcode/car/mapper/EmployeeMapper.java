package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.query.QueryObject;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Employee record);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectAll();

    int updateByPrimaryKey(Employee record);

    List<Employee> selectForList(EmployeeQueryObject qo);

    void batcDeleByds(@Param("ids") Long[] ids);

    Employee selectByUsername(String username);
}