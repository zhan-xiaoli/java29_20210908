package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Employee;
import cn.wolfcode.car.domain.Role;
import cn.wolfcode.car.query.EmployeeQueryObject;
import cn.wolfcode.car.query.QueryObject;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Role record);

    Role selectByPrimaryKey(Long id);

    List<Role> selectAll();

    int updateByPrimaryKey(Role record);
 /* 分页查询*/
    List<Role> selectForList(QueryObject qo);

    List<Role> selectEmployeeid(Long id);

    List<String> getRoleByEmployeeId(Long id);
}