package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.query.QueryObject;

import java.util.List;

public interface DepartmentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Department record);

    Department selectByPrimaryKey(Long id);

    List<Department> selectAll();

    int updateByPrimaryKey(Department record);

    List<Department> selectForList(QueryObject qo);
}
