package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.domain.Permission;
import cn.wolfcode.car.query.QueryObject;

import java.util.List;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Permission record);

    Permission selectByPrimaryKey(Long id);

    List<Permission> selectAll();

    int updateByPrimaryKey(Permission record);
    List<Permission> selectForList(QueryObject qo);

    List<Permission> selectByEmployee(Long id);

    List<Permission> selectByRoleId(Long id);

    List<String> getPermissionExpressionByEmployeeId(Long id);
}