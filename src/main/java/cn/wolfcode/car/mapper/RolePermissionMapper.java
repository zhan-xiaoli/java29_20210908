package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.RolePermission;
import java.util.List;

public interface RolePermissionMapper {
    int insert(RolePermission record);

    List<RolePermission> selectAll();

    void deleteByRole_id(Long id);
}