package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionaryItem;
import cn.wolfcode.car.query.SystemDictionaryItemQueryObject;

import java.util.List;

public interface SystemDictionaryItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionaryItem record);

    SystemDictionaryItem selectByPrimaryKey(Long id);

    List<SystemDictionaryItem> selectAll();

    int updateByPrimaryKey(SystemDictionaryItem record);

    List<SystemDictionaryItem> selectForList(SystemDictionaryItemQueryObject qo);

    List<SystemDictionaryItem> selectParent();

    List<SystemDictionaryItem> selectByTypeId(int i);
}