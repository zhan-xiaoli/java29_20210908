package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Business;
import cn.wolfcode.car.domain.SystemDictionary;
import cn.wolfcode.car.query.QueryObject;

import java.util.List;

public interface SystemDictionaryMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionary record);

    SystemDictionary selectByPrimaryKey(Long id);

    List<SystemDictionary> selectAll();

    int updateByPrimaryKey(SystemDictionary record);

    List<SystemDictionary> selectForList(QueryObject qo);



}