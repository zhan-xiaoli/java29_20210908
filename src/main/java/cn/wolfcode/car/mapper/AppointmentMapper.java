package cn.wolfcode.car.mapper;

import cn.wolfcode.car.domain.Appointment;
import cn.wolfcode.car.domain.Department;
import cn.wolfcode.car.query.AppointmentQueryObject;
import cn.wolfcode.car.query.QueryObject;

import java.util.List;

public interface AppointmentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Appointment record);

    Appointment selectByPrimaryKey(Long id);

    List<Appointment> selectAll();

    int updateByPrimaryKey(Appointment record);
    List<Appointment> selectForList(AppointmentQueryObject qo);
}