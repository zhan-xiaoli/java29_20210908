<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>权限管理</title>
    <#include "/common/link.ftl">
    <script>
        $(function (){
            $("#reoad").click(function (){
                $.ajax({
                    url:'/permission/reload',
                    data:{},
                    success:function (data){
                        if(data.success){
                            Swal.fire({
                                title: '加载成功!',
                                icon:'success',
                                timer:3000
                            }).then((result) => {
                                window.location.href = '/permission/list';
                            });
                        }
                        else{
                            Swal.fire(
                                '加载失败!',
                                '囧.',
                                'error'
                            );
                        }
                    },
                    error:function(){
                        alert(123);
                    }

                });
            });


        })
        $(function(){
            $("#addbtn").click(function (){
                $("#deptid").val("");
                $("#deptname").val("");
                $("#deptexpression").val("");
                $("#myModal").modal("show");
            });

            $("#submitbtn").click(function (){
                $("#saveOrUpdataForm").submit();
            });
            $(".edition").click(function (){
                //回显
                var dept = $(this).data("json");
                $("#deptid").val(dept.id);
                $("#deptname").val(dept.name);
                $("#deptexpression").val(dept.expression);
                $("#myModal").modal("show");
            });
        })


    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="permission"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>权限管理</h1>
        </section>
        <section class="content">
            <div class="box" >
                <!--高级查询--->
                <form class="form-inline" id="searchForm" action="/permission/list" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <a href="javascript:;"  id="reoad" class="btn btn-success btn-reload" style="margin: 10px;">
                        <span class="glyphicon glyphicon-repeat"></span>  重新加载
                    </a>
                    <a href="javascript:void(0);" id="addbtn"  class="btn btn-success btn-input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
                <div class="box-body table-responsive ">
                <table class="table table-hover table-bordered table-striped" >
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>权限名称</th>
                        <th>权限表达式</th>
                        <th>操作</th>
                   
                    </tr>
                    </thead>
                    <tbody>
                    <#list result.list as tmp>
                    <tr>
                        <td>${tmp.id}</td>
                        <td>${tmp.name}</td>
                        <td>${tmp.expression}</td>
                        <td>
                            <a class="btn btn-danger btn-xs btn-delete deletebtn" href="javascript:void(0);" employeeid="${tmp.id}">
                                <span class="glyphicon glyphicon-trash"></span> 删除
                            </a>
                            <a  data-json='${tmp.json}' href="javascript:void(0);" class="btn btn-info btn-xs btn-input edition">
                                <span class="glyphicon glyphicon-pencil"></span> 编辑
                            </a>
                        </td>
                        </tr>
                    </#list>

                    </tbody>
                </table>
                    <!--分页-->
                    <#include "common/page.ftl" >
                </div>
            </div>
        </section>
    </div>
    <#include "common/footer.ftl" >
</div>


<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    新增/编辑
                </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" action="/permission/saveOrUpdate" id="saveOrUpdataForm">
                    <input type="hidden" name="id" id="deptid">
                    <div class="form-group">
                        <label for="firstname" class="col-sm-3 control-label">权限名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="deptname" name="name"
                                   placeholder="权限名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">权限表达式</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="deptexpression" name="expression"
                                   placeholder="权限表达式">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" id="submitbtn">
                    提交更改
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script>
    //单个删除
    $(".deletebtn").click(function (){
        var employeeid = $(this).attr("employeeid");
        Swal.fire({
            title: '确认删除?',
            text: "删除后不能恢复!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url:'/permission/delete',
                    data:{
                        id:employeeid
                    },
                    error:function (){
                    },
                    success:function(data){
                        if(data.success){
                            Swal.fire(
                                '删除成功!',
                                '已成功删除.',
                                'success'
                            );
                            window.location.href = '/permission/list';
                        }
                        else{
                            Swal.fire(
                                '删除失败!',
                                '未成功删除.',
                                'error'
                            );
                        }
                    }
                });

            }
        })
    });

</script>



</body>
</html>
