<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>字典明细管理</title>
    <#-- 使用相对当前模板文件的路径 再去找另一个模板文件 -->
    <#include "/common/link.ftl">
    <script>
        $(function(){
            $(".deletebtn").click(function (){
                var deptid = $(this).attr("deptid");
                Swal.fire({
                    title: '确认删除?',
                    text: "删除后不能恢复!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '确认',
                    cancelButtonText: '取消'
                }).then((result) => {
                    if (result.value) {
                        //点击确认按钮后做的事情
                        $.ajax({
                            url:'/systemDictionaryItem/delete',
                            data:{
                                id:deptid
                            },
                            error:function (){
                            },
                            success:function(data){
                                if(data.success){
                                    Swal.fire(
                                        '删除成功!',
                                        '已成功删除.',
                                        'success'
                                    );
                                    window.location.href = '/systemDictionaryItem/list';
                                }
                                else{
                                    Swal.fire(
                                        '删除失败!',
                                        '未成功删除.',
                                        'error'
                                    );
                                }
                            }
                        });

                    }
                })
            });

        });
    </script>

</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--定义一个变量  菜单回显-->
    <#assign currentMenu="systemDictionaryItem"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>字典明细管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <div class="row" style="margin:20px">
                    <div class="col-xs-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">字典目录</div>
                            <div class="panel-body">
                                <div class="list-group" id="dic">
                                    <#list systemDictionaryList  as tmpq>
                                   <a id="${tmpq.id}" onclick="goSubItem(${tmpq.id})"  href="javascript:void(0);" class="list-group-item">${tmpq.title}</a>
                                        <script>
                                            if ('${tmp.id}'=='${qo.type_id}'){
                                                $("#${tmp.id}").addClass("active");
                                            }
                                        </script>
                                    </#list>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-10">
                        <!--高级查询--->
                        <form class="form-inline" id="searchForm" action="/systemDictionaryItem/list" method="post">
                            <input type="hidden" name="currentPage" id="currentPage" value="1">
                            <input type="hidden" name="type_id" id="hidden_id" value="${qo.type_id}">
                            <a  href="javascript:void(0);"  class="btn btn-success btn-input btninput" style="margin: 10px">
                                <span class="glyphicon glyphicon-plus"></span> 添加
                            </a>
                        </form>
                        <!--编写内容-->
                        <div class="box-body table-responsive no-padding ">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>编号</th>
                                    <th>字典明细标题</th>
                                    <th>字典明细序号</th>
                                    <th>上级明细</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <#list result.list as tmp>
                                <tbody>
                                <tr>
                                    <td>${tmp.id}</td>
                                    <td>
                                        <a href="#">${tmp.title}</a>
                                    </td>
                                    <td>${tmp.sequence}</td>
                                    <td>${tmp.parent.title! '无'}</td>
                                    <td>
                                        <a  data-json='${tmp.json}' href="javascript:void(0);" class="btn btn-info btn-xs btn-input editbtn">
                                            <span class="glyphicon glyphicon-pencil"></span> 编辑
                                        </a>
                                        <a class="btn btn-danger btn-xs btn-delete deletebtn" href="javascript:void(0);" deptid="${tmp.id}">
                                            <span class="glyphicon glyphicon-trash"></span> 删除
                                        </a>
                                    </td>
                                </tr>
                                </#list>
                                </tbody>

                            </table>
                            <#include "/common/page.ftl" >
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl" >
</div>


<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
            </div>
            <form class="form-horizontal" action="/systemDictionaryItem/saveOrUpdate" method="post" id="editForm">
                <div class="modal-body">
                    <input type="hidden" name="id" >
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">字典目录：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="type_id" >
                                <#list systemDictionaryList as tmp>
                                    <option value="${tmp.id}">${tmp.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label  class="col-sm-3 control-label">上级明细：</label>
                        <div class="col-sm-6">
                            <select class="form-control"  name="parent.id" id="parentid">
                                <option value="">无</option>
                                <#list parentitems as tmp>
                                    <option value="${tmp.id}">${tmp.title}</option>
                                </#list>
                                <#--<option value="1">售卖</option>
                                <option value="2">保养</option>
                                <option value="3">修理</option>
                                <option value="4">美容</option>
                                <option value="5">配件</option>
                                <option value="6">订货</option>-->

                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label  class="col-sm-3 control-label">明细标题：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title"
                                   placeholder="请输入字典明细编码">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label class="col-sm-3 control-label">明细序号：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sequence"
                                   placeholder="请输入字典明细序号">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>


    function  goSubItem(id){
        $("input[name=type_id]").val(id);
        $("#searchForm").submit();
    }
    $(".btninput").click(function () {
        $("input[name=id]").val("");
        $("input[name=title]").val("");
        $("input[name=sequence]").val("");
        $("input[name=type_id]").val("");
        $("#parentid").val("");
        $("#editModal").modal("show")
    })
    $(".editbtn").click(function () {
        var json = $(this).data("json");
        $("input[name=id]").val(json.id);
        $("input[name=title]").val(json.title);
        $("input[name=sequence]").val(json.sequence);
        $("select[name=type_id]").val(json.type_id);
        if (json.parent != null)
            $("#parentid").val(json.parent.id);
        $("#editModal").modal("show");
    });

</script>
</body>
</html>
