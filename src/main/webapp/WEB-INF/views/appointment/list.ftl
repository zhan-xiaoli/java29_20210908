<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>预约单管理</title>
    <#include "/common/link.ftl">
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="appointment"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>预约单管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/appointment/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <div class="form-group">
                            <label>预约单流水号</label>
                            <input type="text" class="form-control"
                                   value="${qo.ano}" name="ano"
                                   placeholder="请输入预约单流水号">
                        </div>
                        <div class="form-group">
                            <label>预约业务大类</label>
                            <select class="form-control" id="categoryId" name="category_id">
                                <option value="">请选择业务大类</option>
                                <#list category as tmp>
                                    <option value="${tmp.id}">${tmp.title}</option>
                                </#list>
                            </select>
                            <script>
                                $("#categoryId").val(${qo.category_id});
                            </script>
                        </div>
                        <div class="form-group">
                            <label>预约单状态</label>
                            <select class="form-control" id="status" name="status">
                                <option value="">全部</option>
                                <#list statue as tmp>
                                <#--枚举类型获取name 属性不能直接获取，需要通过get方法-->
                                    <option value="${tmp.getValue()}">${tmp.getName()}</option>
                                </#list>
                            </select>
                            <script>
                                $("#status").val(${qo.status});
                            </script>
                        </div>
                        <div class="form-group">
                            <label>门店查询</label>
                            <select class="form-control" id="businessId" name="business_id">
                                <option value="">请选择门店</option>
                                <#list businesses as tmp>
                                    <option value="${tmp.id}">${tmp.name}</option>
                                </#list>
                            </select>
                            <script>
                                $("#businessId").val(${qo.business_id});
                            </script>
                        </div>
                        <div class="form-group">
                            <label>客户名称</label>
                            <input type="text" class="form-control"
                                   name="contact_name" value="${qo.contact_name}"
                                   placeholder="请输入客户名称">
                        </div>

                        <div class="form-group">
                            <label>客户手机号</label>
                            <input type="text" class="form-control"
                                   name="contact_tel" value="${qo.contact_tel}"
                                   placeholder="请输入客户手机号">
                        </div>

                        <br/>
                        <br/>

                        <div class="form-group">
                            <label>预约时间查询：</label>
                            <input placeholder="请输入开始时间" type="text"
                                   name="startDate" value="${(qo.startDate?string('yyyy-MM-dd HH:mm'))!}"
                                   class="form-control input-date"/> -
                            <input placeholder="请输入结束时间" type="text"
                                   name="endDate" value="${(qo.endDate?string('yyyy-MM-dd HH:mm'))!}"
                                   class="form-control input-date"/>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-search"></span>
                            查询
                        </button>

                        <a href="javascript:void(0);" class="btn btn-success btn-input btninput">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                    </form>

                </div>
                <div class="box-body table-responsive ">
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>流水号</th>
                            <th>业务大类</th>
                            <th>预约说明</th>
                            <th>预约时间</th>
                            <th>客户名称</th>
                            <th>联系方式</th>
                            <th>预约门店</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list result.list as tmp>
                            <tr>
                                <td>${tmp_index+1}</td>
                                <td>${tmp.ano}</td>
                                <td>${tmp.category.title}</td>
                                <td>${tmp.info}</td>
                                <td>${(tmp.appointment_time?string("yyyy-MM-dd hh:mm"))!}</td>
                                <td>${tmp.contact_name}</td>
                                <td>${tmp.contact_tel}</td>
                                <td>${tmp.business.name}</td>
                                <td>${tmp.statusNmae}</td>
                                <td>
                                    <a href="javascript:void(0);" data-json='${tmp.json}'
                                       class="btn btn-info btn-xs btn-input btnedit">
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <@shiro.hasPermission name="appointment:delete">
                                    <a class="btn btn-danger btn-xs btn-delete deletebtn" href="javascript:void(0);" tmptid="${tmp.id}">
                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                    </a>
                                    </@shiro.hasPermission>
                                    <a class="btn btn-xs btn-primary btn-status" href="javascript:void(0);"
                                       onclick="PERFORM(${tmp.id},1);">
                                        <span class="glyphicon glyphicon-phone-alt"></span> 确认预约
                                    </a>


                                    <a class="btn btn-xs btn-primary btn-status" href="javascript:void(0);"
                                       onclick="FAILURE(${tmp.id},4);">
                                        <span class="glyphicon glyphicon-phone-alt"></span> 取消预约
                                    </a>


                                    <a class="btn btn-xs btn-primary btn-status" href="javascript:void(0);"
                                       onclick="update_appointment(${tmp.id},2);">
                                        <span class="glyphicon glyphicon-phone-alt"></span> 确认到店
                                    </a>
                                    <a class="btn btn-xs btn-primary btn-status" href="javascript:void(0);"
                                       onclick="FINISH(${tmp.id},3);">
                                        <span class="glyphicon glyphicon-phone-alt"></span> 消费完成
                                    </a>
                                </td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    <!--分页-->
                    <#include "/common/page.ftl">
                </div>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>


<#-- 文件上传模态框 -->
<!--模态框-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="editForm" action="/appointment/saveOrUpdate" method="post">
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">预约门店：</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="business.id">
                                <option value="">请选择预约门店</option>
                                <#list businesses as tmp>
                                    <option value="${tmp.id}">${tmp.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">预约时间：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-date"
                                   name="appointment_time"
                                   placeholder="请输入预约时间"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">业务大类：</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="category.id">
                                <option value="">请选择业务大类</option>
                                <#list category as tmp>
                                    <option value="${tmp.id}">${tmp.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">联系人：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   name="contact_name" id="contact_name"
                                   placeholder="请输入联系人">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">联系电话：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   name="contact_tel" id="contact_tel"
                                   placeholder="请输入联系电话">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">预约说明：</label>
                        <div class="col-sm-7">
                            <textarea type="text" class="form-control" name="info"
                                      placeholder="请输入预约说明"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

    $('.input-date').datetimepicker({
        format:'yyyy-mm-dd hh:ii', //格式
        language:'zh-CN', //中文
        autoclose: true,//选择后自动关闭
        //showMeridian:true, //是否显示上下午
        minView:0,//精确到哪位
    });

    // 调模态框
    $(".btninput").click(function (){
        $("#editModal").modal("show");
    });
    // 回显
    $(".btnedit").click(function (){
        var json = $(this).data("json");
        $("#business").val(json.business_id);
        $("input[name=appointment_time]").val(json.appointment_time);
        $("#category").val(json.category_id);
        $("input[id=contact_name]").val(json.contact_name);
        $("input[id=contact_tel]").val(json.contact_tel);
        $("textarea[name=info]").html(json.info);
        $("input[name=id]").val(json.id);
        $("#editModal").modal("show");

    });
    // 修改模态框的状态 传人要修改的id 和状态码
    function update_appointment(id, status) {
        Swal.fire({
            title: '确认预约?',
            text: "是否已和客户确认预约信息",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url: '/appointment/saveOrUpdate_async',
                    data: {
                        id: id,
                        status: status
                    },
                    success: function (data) {
                        if (data.success) {
                            Swal.fire(
                                '预约成功!',
                                '已成功预约.',
                                'success'
                            );
                            $("#currentPage").val(${qo.currentPage});
                            $("#searchForm").submit();
                        } else {
                            Swal.fire(
                                '预约失败!',
                                '未成功预约.',
                                'error'
                            );
                        }
                    },
                    error: function () {
                        alert("error appointment");
                    }
                });

            }
        });
    }





    $(".deletebtn").click(function (){
        var tmptid = $(this).attr("tmptid");
        Swal.fire({
            title: '确认删除?',
            text: "删除后不能恢复!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url:'/appointment/delete',
                    data:{
                        id:tmptid
                    },
                    error:function (){
                    },
                    success:function(data){
                        if(data.success){
                            Swal.fire(
                                '删除成功!',
                                '已成功删除.',
                                'success'
                            );
                            window.location.href = '/appointment/list';
                        }
                        else{
                            Swal.fire(
                                '删除失败!',
                                '未成功删除.',
                                'error'
                            );
                        }
                    }
                });

            }
        })
    });

// 确认预约
    function PERFORM(id, status) {
        Swal.fire({
            title: '确认预约?',
            text: "是否已和客户确认预约信息",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url: '/appointment/saveOrUpdate_async',
                    data: {
                        id: id,
                        status: status
                    },
                    success: function (data) {
                        if (data.success) {
                            Swal.fire(
                                '预约成功!',
                                '已成功预约.',
                                'success'
                            );
                            $("#currentPage").val(${qo.currentPage});
                            $("#searchForm").submit();
                        } else {
                            Swal.fire(
                                '预约失败!',
                                '未成功预约.',
                                'error'
                            );
                        }
                    },
                    error: function () {
                        alert("error appointment");
                    }
                });

            }
        });
    }

    // 取消预约
    function FAILURE(id, status) {
        Swal.fire({
            title: '是否取消预约?',
            text: "是否已和客户确认预约信息",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url: '/appointment/saveOrUpdate_async',
                    data: {
                        id: id,
                        status: status
                    },
                    success: function (data) {
                        if (data.success) {
                            Swal.fire(
                                '取消预约成功!',
                                '已成功取消预约.',
                                'success'
                            );
                            $("#currentPage").val(${qo.currentPage});
                            $("#searchForm").submit();
                        } else {
                            Swal.fire(
                                '取消预约失败!',
                                '未成功取消预约.',
                                'error'
                            );
                        }
                    },
                    error: function () {
                        alert("error appointment");
                    }
                });

            }
        });
    }



    // 以消费
    function FINISH(id, status) {
        Swal.fire({
            title: '确认付款?',
            text: "是否已和客户确认金额信息",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url: '/appointment/saveOrUpdate_async',
                    data: {
                        id: id,
                        status: status
                    },
                    success: function (data) {
                        if (data.success) {
                            Swal.fire(
                                '付款成功!',
                                '已成功付款.',
                                'success'
                            );
                            $("#currentPage").val(${qo.currentPage});
                            $("#searchForm").submit();
                        } else {
                            Swal.fire(
                                '付款失败!',
                                '未成功付款.',
                                'error'
                            );
                        }
                    },
                    error: function () {
                        alert("error appointment");
                    }
                });

            }
        });
    }


</script>



</body>
</html>
