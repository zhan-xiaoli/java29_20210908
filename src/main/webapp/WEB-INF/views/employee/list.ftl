<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理</title>
    <#include "/common/link.ftl">

</head>
<#-- 调入的模态框功能,通过下载以及上传文件-->

<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">导入</h4>
            </div>

            <form class="form-horizontal" action="/employee/importXls" enctype="multipart/form-data" method="post" id="importForm">
                <div class="modal-body">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <!-- 文件上传框 -->
                            <input type="file" name="file">
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <a href="/employee/export_model" class="btn btn-success" >
                                <span class="glyphicon glyphicon-download"></span> 下载模板
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="employee"/>
    <#include "/common/menu.ftl">

    <div class="content-wrapper">
        <section class="content-header">
            <h1>员工管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 10px;">
                    <form class="form-inline" id="searchForm" action="/employee/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="${qo.currentPage}">
                        <div class="form-group">
                            <label for="keywords">关键字:</label>
                            <input value="${qo.keywords}" type="text" class="form-control" name="keywords"
                                   placeholder="请输入姓名/邮箱">
                        </div>
                        <div class="form-group">
                            <label for="dept"> 部门:</label>
                            <select class="form-control" id="dept" name="deptId">
                                <option value="">全部</option>
                                <#list deptList as tmp>
                                    <option value="${tmp.id}">${tmp.name}</option>
                                </#list>
                            </select>
                            <script>
                                $("#dept").val(${qo.deptId});
                            </script>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-search"></span>
                            查询
                        </button>
                        <a href="/employee/input" class="btn btn-success btn-input">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>


                        <a href="javascript:void(0);" class="btn btn-danger btn-input batchdelete">
                            <span class="glyphicon glyphicon-trash"></span> 批量删除
                        </a>

                        <a href="/employee/export" class="btn btn-warning btn-input ">
                            <span class="glyphicon glyphicon-download"></span> 导出
                        </a>



                        <a href="javascript:void(0);" class="btn btn-danger btn-warning" id="importxls">
                            <span class="glyphicon glyphicon-upload"></span> 导入
                        </a>



                    </form>
                </div>
                <div class="box-body table-responsive ">
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="allCb"></th>
                            <th>编号</th>
                            <th>用户名</th>
                            <th>真实姓名</th>
                            <th>邮箱</th>
                            <th>年龄</th>
                            <th>部门</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list result.list as tmp>
                            <tr>
                                <td><input type="checkbox" class="cb" value="${tmp.id}"></td>
                                <td>${tmp.id}</td>
                                <td>${tmp.username}</td>
                                <td>${tmp.name}</td>
                                <td>${tmp.email}</td>
                                <td>${tmp.age}</td>
                                <td>${tmp.dept.name}</td>
                                <td>
                                    <a href="/employee/input?id=${tmp.id}"
                                       class="btn btn-info btn-xs btn_redirect">
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑

                                    </a>
                                    <@shiro.hasPermission name="department:delete">
                                    <a href="javascript:void(0);" employeeid="${tmp.id}"
                                       class="btn btn-danger btn-xs btn-delete deletebtn">
                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                    </a>
                                    </@shiro.hasPermission>
                                </td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    <!--分页-->
                    <#include "/common/page.ftl">
                </div>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>
<script>

    //单个删除
    $(".deletebtn").click(function () {
        var employeeid = $(this).attr("employeeid");
        Swal.fire({
            title: '确认删除?',
            text: "删除后不能恢复!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url: '/employee/delete',
                    data: {
                        id: employeeid
                    },
                    error: function () {

                    },
                    success: function (data) {
                        if (data.success) {
                            Swal.fire({
                                title: '删除成功!',
                                text: '已成功删除.',
                                icon: 'success',
                                timer: 3000
                            }).then((result) => {
                                window.location.href = '/employee/list';
                            });

                        } else {
                            Swal.fire(
                                '删除失败!',
                                '未成功删除.',
                                'error'
                            );
                        }
                    }
                });

            }
        })
    });


    //批量删除
    $("#allCb").change(function () {
        var flg = $(this).prop("checked");
        $('.cb').prop("checked", flg);
    });


    $(".batchdelete").click(function () {
        var cbs = $(".cb:checked");
        var arr = [];
        for (var i = 0; i < cbs.size(); i++) {
            arr.push($(cbs[i]).val());
        }

        Swal.fire({
            title: '确认删除多条记录?',
            text: "删除后不能恢复!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url: '/employee/batchdelete',
                    data: {
                        ids: arr
                    },
                    //传递数组需要设置为true
                    traditional: true,
                    error: function () {
                    },
                    success: function (data) {
                        if (data.success) {
                            Swal.fire({
                                title: '删除成功!',
                                text: '已成功删除.',
                                icon: 'success',
                                timer: 3000
                            }).then((result) => {
                                window.location.href = '/employee/list';
                            });
                        } else {
                            Swal.fire(
                                '删除失败!',
                                '未成功删除.',
                                'error'
                            );
                        }
                    }
                });
            }
        })
    });

/*    调入的模态框的调用*/
    //    模态框
    $('#importxls').click(function () {
        $('#importModal').modal("show");
    })

</script>

</body>
</html>
