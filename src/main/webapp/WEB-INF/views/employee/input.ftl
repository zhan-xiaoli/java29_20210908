﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理</title>
    <#include "/common/link.ftl">
    <script>
        var x;
        $(function () {
            $("#admin").change(function () {
                var flg = $(this).prop("checked");
                if (flg) {
                    x = $("#roleDiv").detach();
                } else {
                    $("#adminDiv").after(x);
                }
            });
            $(".btn-submit").click(function () {
                $(".selfRoles>option").prop("selected", true);
                //提交表单
               // $("#editForm").submit();
                document.getElementById("editForm").submit();
            });


        });

        function moveSelected(src, dest) {
            //$("#xxxx>option:selected")
            $("." + dest).append($("." + src + ">option:selected"));
        }

        function moveAll(src, dest) {
            //$("#xxxx>option")
            $("." + dest).append($("." + src + ">option"));
        }


    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="employee"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>员工编辑</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-horizontal" action="/employee/saveOrUpdate" method="post" id="editForm">
                    <input type="hidden" name="id" value="${curremp.id}">
                    <div class="form-group" style="margin-top: 10px;">
                        <label class="col-sm-2 control-label">用户名：</label>
                        <div class="col-sm-6">
                            <input value="${curremp.username}" type="text" class="form-control" name="username"
                                   placeholder="请输入用户名">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label class="col-sm-2 control-label">真实姓名：</label>
                        <div class="col-sm-6">
                            <input value="${curremp.name}" type="text" class="form-control" name="name"
                                   placeholder="请输入真实姓名">
                        </div>
                    </div>

                    <#if !curremp??>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">密码：</label>
                            <div class="col-sm-6">
                                <input value="${curremp.password}" type="password" class="form-control" id="password"
                                       name="password" placeholder="请输入密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="repassword" class="col-sm-2 control-label">验证密码：</label>
                            <div class="col-sm-6">
                                <input type="password" value="${curremp.password}" class="form-control" id="repassword"
                                       name="repassword" placeholder="再输入一遍密码">
                            </div>
                        </div>
                    </#if>


                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">电子邮箱：</label>
                        <div class="col-sm-6">
                            <input value="${curremp.email}" type="text" class="form-control" name="email"
                                   placeholder="请输入邮箱">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="age" class="col-sm-2 control-label">年龄：</label>
                        <div class="col-sm-6">
                            <input type="text" value="${curremp.age}" class="form-control" name="age"
                                   placeholder="请输入年龄">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="dept" class="col-sm-2 control-label">部门：</label>
                        <div class="col-sm-6">
                            <select class="form-control" id="dept" name="dept.id">
                                <option value="">全部</option>
                                <#list deptList as tmp>
                                    <option value="${tmp.id}">${tmp.name}</option>
                                </#list>
                            </select>
                            <script>
                                $("#dept").val(${curremp.dept.id});
                            </script>
                        </div>
                    </div>
                    <div class="form-group" id="adminDiv">
                        <label for="admin" class="col-sm-2 control-label">超级管理员：</label>
                        <div class="col-sm-6" style="margin-left: 15px;">
                            <input value="1" type="checkbox" id="admin" name="admin" class="checkbox">
                        </div>
                    </div>
                    <div class="form-group" id="roleDiv">
                        <label for="role" class="col-sm-2 control-label">分配角色：</label><br/>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-sm-2 col-sm-offset-2">
                                <select multiple class="form-control allRoles" size="15">
                                    <#list roleList as tmp>
                                        <option value="${tmp.id}">${tmp.name}</option>
                                    </#list>
                                </select>
                            </div>

                            <div class="col-sm-1" style="margin-top: 60px;" align="center">
                                <div>

                                    <a type="button" class="btn btn-primary  " style="margin-top: 10px" title="右移动"
                                       onclick="moveSelected('allRoles', 'selfRoles')">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="左移动"
                                       onclick="moveSelected('selfRoles', 'allRoles')">
                                        <span class="glyphicon glyphicon-menu-left"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全右移动"
                                       onclick="moveAll('allRoles', 'selfRoles')">
                                        <span class="glyphicon glyphicon-forward"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全左移动"
                                       onclick="moveAll('selfRoles', 'allRoles')">
                                        <span class="glyphicon glyphicon-backward"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <select multiple class="form-control selfRoles" size="15" name="ids">
                                    <#list selfRoles as tmp>
                                        <option value="${tmp.id}">${tmp.name}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(function () {
                            var $options = $(".selfRoles>option");
                            var arr = [];
                            $options.each(function (i, that) {
                                arr.push($(that).val());
                            });
                            $(".allRoles>option").each(function (i, that) {
                                if (arr.indexOf($(that).val()) >= 0) {
                                    $(that).remove();
                                }
                            });
                        });
                    </script>
                    <#--调整超管为true的情况-->
                    <script>
                        $("#admin").prop("checked", ${curremp.admin});
                        if ('${curremp.admin}' == 'true') {
                            x = $("#roleDiv").detach();
                        }
                    </script>
                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button type="button" class="btn btn-primary btn-submit">保存</button>
                            <a href="javascript:window.history.back()" class="btn btn-danger">取消</a>
                        </div>
                    </div>

                </form>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>
</body>
<script>
    $("#editForm").bootstrapValidator({
        //图标
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        //配置要验证的字段
        fields: {
            username: {
                //对当前的字段设定验证规则
                validators: {
                    //不为空
                    notEmpty: {
                        //错误时的提示信息
                        message: "用户名必填"
                    },
                    //字符串的长度范围
                    stringLength: {
                        min: 1,
                        max: 10
                    },
                    //可以提交远程验证
                    remote: {
                        type: 'POST',
                        url: '/employee/checkName',
                        //验证不通过时的提示信息
                        message: '用户名已存在',
                        //取值单位是 S 毫秒  当你在输入框中输入内容时
                        // 多少秒后开始发送请求进行验证
                        delay: 20
                    }
                }
            },

            name: {
                validators: {
                    notEmpty: {
                        //错误时的提示信息
                        message: "真实姓名不能为空"
                    },
                    stringLength: {
                        min: 1,
                        max: 20
                    }
                }
            },

            password: {
                validators: {
                    notEmpty: {
                        //错误时的提示信息
                        message: "密码必填"
                    }
                }
            },

            repassword: {
                validators: {
                    notEmpty: {
                        //错误时的提示信息
                        message: "密码必填"
                    },
                    //两个字段的值需要相等
                    identical: {
                        field: 'password',
                        message: '两次密码不相等'
                    }
                }
            },
            email: {
                validator: {
                    emailAddress: {
                        message: '邮箱格式不正确'
                    }
                }
            },
            age: {
                validators: {
                    between: {
                        min: 18,
                        max: 65
                    }
                }
            }
        }
        //表单所有数据验证通过后执行里面的代码
    }).on('success.form.bv', function () {
        $(".selfRoles>option").prop("selected", true);
    });



</script>
</html>
