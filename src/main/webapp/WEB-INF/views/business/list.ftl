<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>门店管理</title>
    <#include "/common/link.ftl">
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="business"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>门店管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/business/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                                <div class="form-group">
                                    <label for="name">门店名称：</label>
                                    <input type="text" class="form-control" name="name" value="${qo.name}"
                                           placeholder="请输入门店名称">
                                </div>
                                <div class="form-group">
                                    <label for="scope">经营范围：</label>
                                    <input type="text" class="form-control" name="scope" value="${qo.scope}"
                                           placeholder="请输入经营范围">
                                </div>
                                <div class="form-group">
                                    <label for="tel">门店联系方式：</label>
                                    <input type="text" class="form-control" name="lel" value="${qo.tel}"
                                           placeholder="请输入门店联系方式">
                                </div>
                                <div class="form-group">
                                    <label for="legalName">根据法人查询：</label>
                                    <input type="text" class="form-control"  name="legal_name" value="${qo.legal_name}"
                                           placeholder="请输入法人姓名">
                                </div>

                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label>经营日期查询：</label>
                                    <input value="${(qo.startDate?string("yyyy-MM-dd"))!}" id="startDate" name="startDate"
                                           placeholder="请输入开始日期" type="text" class="form-control input-date"/> -
                                    <input value="${(qo.endDate?string("yyyy-MM-dd"))!}" id="endDate" name="endDate"
                                           placeholder="请输入结束日期" type="text" class="form-control input-date"/>
                                </div>
                                <script>
                                    $('.input-date').datetimepicker({
                                        // 格式
                                        format:'yyyy-mm-dd',//格式
                                        language:'zh-CN', // 中文
                                        autoclose: true ,// 关闭时间cj
                                         showMeridian: true, // 显示上下午
                                        minView:2
                                    })
                                </script>

                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 查询</button>

                                <a href="/business/input" class="btn btn-success">
                                    <span class="glyphicon glyphicon-plus"></span> 添加
                                </a>
                    </form>

                </div>
                <div class="box-body table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>门店名称</th>
                        <th>门店电话</th>
                        <th>门店地址</th>
                        <th>经营时间</th>
                        <th>门店性质</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list result.list as emp>
                        <tr>
                         <td>${emp.id}</td>
                           <td>${emp.name}</td>
                           <td>${emp.tel}</td>
                           <td>${emp.address}</td>
                           <td>${(emp.open_date?string('yyyy-MM-dd hh:mm:ss'))!}</td>
                           <td>${(emp.main_store)?string('总店','分店')}</td>
                           <td>
                               <a href="/business/input?id=${emp.id}" class="btn btn-info btn-xs">
                                   <span class="glyphicon glyphicon-pencil"></span> 编辑
                               </a>
                               <@shiro.hasPermission name="business:delete">
                               <a class="btn btn-danger btn-xs btn-delete deletebtn" href="javascript:void(0);" enptid="${emp.id}">
                                   <span class="glyphicon glyphicon-trash"></span> 删除
                               </a>
                               </@shiro.hasPermission>
                           </td>

                          <#-- <td>2</td>
                           <td>狼途汽车服务-深圳福田店</td>
                           <td>020-85628002</td>
                           <td>广州市天河区棠下大地工业园D栋603</td>
                           <td>2020-09-12</td>
                           <td>分店</td>
                           <td>
                               <a href="/business/input.do" class="btn btn-info btn-xs">
                                   <span class="glyphicon glyphicon-pencil"></span> 编辑
                               </a>
                           </td>
                       </tr>
                       <tr>
                           <td>3</td>
                           <td>狼途汽车服务-广州黄埔店</td>
                           <td>020-85628002</td>
                           <td>广州市天河区棠下大地工业园D栋603</td>
                           <td>2020-10-09</td>
                           <td>分店</td>
                           <td>
                               <a href="/business/input.do" class="btn btn-info btn-xs">
                                   <span class="glyphicon glyphicon-pencil"></span> 编辑
                               </a>
                           </td>-->
                        </tr>
                    </#list>
                    </tbody>
                </table>
                <#include "/common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>
<script>
    $(".deletebtn").click(function (){
        var enptid = $(this).attr("enptid");
        Swal.fire({
            title: '确认删除?',
            text: "删除后不能恢复!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确认',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.value) {
                //点击确认按钮后做的事情
                $.ajax({
                    url:'/business/delete',
                    data:{
                        id:enptid
                    },
                    error:function (){
                    },
                    success:function(data){
                        if(data.success){
                            Swal.fire(
                                '删除成功!',
                                '已成功删除.',
                                'success'
                            );
                            window.location.href = '/business/list';
                        }
                        else{
                            Swal.fire(
                                '删除失败!',
                                '未成功删除.',
                                'error'
                            );
                        }
                    }
                });

            }
        })
    });


</script>


</body>
</html>
