<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>字典目录管理</title>
    <#include "/common/link.ftl">
    <script>
        $(function(){
            $("#input").click(function (){
                $("#deptid").val("");
                $("#deptsn").val("");
                $("#depttitle").val("");
                $("#deptintro").val("");
                $("#myModal").modal("show");
            });


            $(".editbtn").click(function (){
                //回显
                var dept = $(this).data("json");
                $("#deptid").val(dept.id);
                $("#deptsn").val(dept.sn);
                $("#depttitle").val(dept.title);
                $("#deptintro").val(dept.intro);
                // 掉模态框
                $("#editModal").modal("show");
            });

            $(".deletebtn").click(function (){
                var deptid = $(this).attr("deptid");
                Swal.fire({
                    title: '确认删除?',
                    text: "删除后不能恢复!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '确认',
                    cancelButtonText: '取消'
                }).then((result) => {
                    if (result.value) {
                        //点击确认按钮后做的事情
                        $.ajax({
                            url:'/systemDictionary/delete',
                            data:{
                                id:deptid
                            },
                            error:function (){
                            },
                            success:function(data){
                                if(data.success){
                                    Swal.fire(
                                        '删除成功!',
                                        '已成功删除.',
                                        'success'
                                    );
                                    window.location.href = '/systemDictionary/list';
                                }
                                else{
                                    Swal.fire(
                                        '删除失败!',
                                        '未成功删除.',
                                        'error'
                                    );
                                }
                            }
                        });

                    }
                })
            });

        });
    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">

    <#assign currentMenu="systemDictionary"/>

    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>字典目录管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <form class="form-inline" id="searchForm" action="/systemDictionary/list" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <input type="hidden" name="type_id" id="type_id">
                    <a href="javascript:void(0);" class="btn btn-success btn-input input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
                <!--编写内容-->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>字典目录标题</th>
                                <th>字典目录编码</th>
                                <th>字典目录简介</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <#list result.list as tmp>
                        <tr>
                            <td>${tmp.id}</td>
                            <td>${tmp.sn}</td>
                            <td>${tmp.title}</td>
                            <td>${tmp.intro}</td>
                                <td>
                                    <a  data-json='${tmp.json}' href="javascript:void(0);" class="btn btn-info btn-xs btn-input editbtn">
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a class="btn btn-danger btn-xs btn-delete deletebtn" href="javascript:void(0);" deptid="${tmp.id}">
                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                    </a>
                                </td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    <#include "/common/page.ftl" >
                </div>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl" >
</div>




<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
            </div>
            <form class="form-horizontal" action="/systemDictionary/saveOrUpdate" method="post" id="editForm">
                <div class="modal-body">
                        <input type="hidden" name="id">
                        <div class="form-group" style="margin-top: 10px;">
                            <label for="name" class="col-sm-3 control-label">名称：</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="sn" id="deptsn"
                                       placeholder="请输入字典目录标题">
                            </div>
                        </div>
                        <div class="form-group" style="margin-top: 10px;">
                            <label for="sn" class="col-sm-3 control-label">编码：</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="title" id="depttitle"
                                       placeholder="请输入字典目录编码">
                            </div>
                        </div>
                        <div class="form-group" style="margin-top: 10px;">
                            <label for="sn" class="col-sm-3 control-label">简介：</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="intro" id="deptintro"
                                       placeholder="请输入字典目录简介">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    // 添加
    $(".input").click(function (){
        $("#editModal").modal("show");
    })
    // 编辑

</script>

</body>
</html>
