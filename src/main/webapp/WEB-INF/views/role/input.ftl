

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>角色管理</title>
    <#include "/common/link.ftl">

    <script>
        var x;
        $(function () {
            $("#admin").change(function () {
                var flg = $(this).prop("checked");
                if (flg) {
                    x = $("#roleDiv").detach();
                } else {
                    $("#adminDiv").after(x);
                }
            });
            $(".btn-submit").click(function () {
                $(".selfPermissions>option").prop("selected", true);
                //提交表单
                $("#editForm").submit();
            });


        });

        function moveSelected(src, dest) {
            //$("#xxxx>option:selected")
            $("." + dest).append($("." + src + ">option:selected"));
        }

        function moveAll(src, dest) {
            //$("#xxxx>option")
            $("." + dest).append($("." + src + ">option"));
        }


    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="role"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>角色编辑</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-horizontal" action="/role/saveOrUpdate" method="post" id="editForm">

                    <input type="hidden"  name="id" value="${role.id}">
                    <div class="form-group"  style="margin-top: 10px;">
                        <label  class="col-sm-2 control-label">角色名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control"  name="name" value="${role.name}" placeholder="请输入角色名称">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">角色编号：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control"  name="sn" value="${role.sn}" placeholder="请输入角色编号">
                        </div>
                    </div>
                    <div class="form-group " id="role">
                        <label for="role" class="col-sm-2 control-label">分配权限：</label><br/>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-sm-2 col-sm-offset-2">
                                <select multiple class="form-control allPermissions" id="permission" name="permission_id" size="15">
                                    <#list permList as tmp >
                                        <option value="${tmp.id}">${tmp.name}</option>
                                    </#list>

                                    <#--  <option value="18">部门编辑</option>
                                      <option value="19">部门删除</option> -->
                                </select>

                                <script>
                                    $("#permission").val(${role.permission.name});
                                </script>
                            </div>

                            <div class="col-sm-1" style="margin-top: 60px;" align="center">
                                <div>

                                    <a type="button" class="btn btn-primary" style="margin-top: 10px" title="右移动"
                                       onclick="moveSelected('allPermissions', 'selfPermissions')">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="左移动"
                                       onclick="moveSelected('selfPermissions', 'allPermissions')">
                                        <span class="glyphicon glyphicon-menu-left"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全右移动"
                                       onclick="moveAll('allPermissions', 'selfPermissions')">
                                        <span class="glyphicon glyphicon-forward"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全左移动"
                                       onclick="moveAll('selfPermissions', 'allPermissions')">
                                        <span class="glyphicon glyphicon-backward"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <select multiple class="form-control selfPermissions" size="15" name="ids">
                                    <#list list as tmp>
                                        <option value="${tmp.id}">${tmp.name}</option>
                                    </#list>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button type="button" class="btn btn-primary btn-submit">保存</button>
                            <a href="javascript:window.history.back()" class="btn btn-danger">取消</a>
                        </div>
                    </div>
                    <script>
                        $(function () {
                            var $options = $(".selfPermissions>option");
                            var arr = [];
                            $options.each(function (i, that) {
                                arr.push($(that).val());
                            });
                            $(".allPermissions>option").each(function (i, that) {
                                if (arr.indexOf($(that).val()) >= 0) {
                                    $(that).remove();
                                }
                            });
                        });
                    </script>
                </form>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>
</body>
</html>
