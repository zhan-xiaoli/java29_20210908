<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>角色管理</title>
    <#include "/common/link.ftl">
    <script>
        $(function(){
            $(".deletebtn").click(function (){
                var deptid = $(this).attr("deptid");
                Swal.fire({
                    title: '确认删除?',
                    text: "删除后不能恢复!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '确认',
                    cancelButtonText: '取消'
                }).then((result) => {
                    if (result.value) {
                        //点击确认按钮后做的事情
                        $.ajax({
                            url:'/role/delete',
                            data:{
                                id:deptid
                            },
                            error:function (){
                            },
                            success:function(data){
                                if(data.success){
                                    Swal.fire(
                                        '删除成功!',
                                        '已成功删除.',
                                        'success'
                                    );
                                    window.location.href = '/role/list';
                                }
                                else{
                                    Swal.fire(
                                        '删除失败!',
                                        '未成功删除.',
                                        'error'
                                    );
                                }
                            }
                        });

                    }
                })
            });

        });
    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="role"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>角色管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 10px;">
                    <!--高级查询--->
                    <form class="form-inline" id="searchForm" action="/role/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <a href="/role/input" class="btn btn-success btn-input"><span class="glyphicon glyphicon-plus"></span> 添加</a>
                    </form>
                    <div class="box-body table-responsive ">
                    <table class="table table-hover table-bordered table-striped" >
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>角色名称</th>
                                <th>角色编号</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        <#list result.list as tmp>
                            <tr>
                                <td>${tmp.id}</td>
                                <td>${tmp.name}</td>
                                <td>${tmp.sn}</td>
                                <td>
                                    <a href="/role/input?id=${tmp.id}" class="btn btn-info btn-xs btn-input">
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                        <a class="btn btn-danger btn-xs btn-delete deletebtn" href="javascript:void(0);" deptid="${tmp.id}">
                                            <span class="glyphicon glyphicon-trash"></span> 删除
                                        </a>
                                </td>
                            </tr>
                        </#list>




                                    <#--                        <tr>-->
<#--                            <td>1</td>-->
<#--                            <td>人事管理</td>-->
<#--                            <td>HR_MGR</td>-->
<#--                            <td>-->
<#--                                <a href="/role/input?id=1" class="btn btn-info btn-xs btn-input">-->
<#--                                    <span class="glyphicon glyphicon-pencil"></span> 编辑-->
<#--                                </a>-->
<#--                                <a  class="btn btn-danger btn-xs btn-delete" >-->
<#--                                    <span class="glyphicon glyphicon-trash"></span> 删除-->
<#--                                </a>-->
<#--                            </td>-->
<#--                        </tr>-->
<#--                        <tr>-->
<#--                            <td>2</td>-->
<#--                            <td>采购管理</td>-->
<#--                            <td>ORDER_MGR</td>-->
<#--                            <td>-->
<#--                                <a href="/role/input?id=2" class="btn btn-info btn-xs ">-->
<#--                                    <span class="glyphicon glyphicon-pencil"></span> 编辑-->
<#--                                </a>-->
<#--                                <a  class="btn btn-danger btn-xs btn-delete" >-->
<#--                                    <span class="glyphicon glyphicon-trash"></span> 删除-->
<#--                                </a>-->
<#--                            </td>-->
<#--                        </tr>-->
<#--                        <tr>-->
<#--                            <td>3</td>-->
<#--                            <td>仓储管理</td>-->
<#--                            <td>WAREHOUSING_MGR</td>-->
<#--                            <td>-->
<#--                                <a href="/role/input?id=3" class="btn btn-info btn-xs ">-->
<#--                                    <span class="glyphicon glyphicon-pencil"></span> 编辑-->
<#--                                </a>-->
<#--                                <a  class="btn btn-danger btn-xs btn-delete" >-->
<#--                                    <span class="glyphicon glyphicon-trash"></span> 删除-->
<#--                                </a>-->
<#--                            </td>-->
<#--                        </tr>-->

                        </tbody>
                    </table>
                    <!--分页-->
                    <#include "/common/page.ftl">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>
</body>
</html>
